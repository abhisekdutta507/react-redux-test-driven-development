import { actionTypes } from "../actions";

/**
 * @function secretWordReducer
 * @param {Array} state - Array of guessed words.
 * @param {object} action - action to be reduced.
 * @returns {boolean} - new success state.
 */
const secretWordReducer = (state='', action) => {
  switch(action.type) {
    case actionTypes.SET_SECRET_WORD: {
      return action.payload
    }
    default: {
      return state
    }
  }
}

export default secretWordReducer