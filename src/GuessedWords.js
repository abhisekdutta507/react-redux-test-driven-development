import React from 'react'
import PropTypes from 'prop-types'

const GuessedWords = (props = {}) => {
  const { guessedWords } = props

  const guessedWordsTable = (words) => {
    return (
      <table className="table table-sm">
        <thead className="thead-light">
          <tr><th>#</th><th>Guess</th><th>Matching Letters</th></tr>
        </thead>
        <tbody>
          {
            words.map((word, index) => (
              <tr key={index} data-test="guessed-word">
                <td>{index + 1}</td>
                <td>{word.guessedWord}</td>
                <td>{word.letterMatchCount}</td>
              </tr>
            ))
          }
        </tbody>
      </table>
    )
  }

  return (
    <div data-test="component-guessed-words">
      {
        !guessedWords?.length ?
          <span data-test="guess-instructions">Try to guess the secret word!</span> :
          <div data-test="guessed-words">
            <h3>Guessed Words</h3>
            {guessedWordsTable(guessedWords)}
          </div>
      }
    </div>
  )
}

GuessedWords.propTypes = {
  guessedWords: PropTypes.arrayOf(
    PropTypes.shape({
      guessedWord: PropTypes.string.isRequired,
      letterMatchCount: PropTypes.number.isRequired
    })
  ).isRequired
}

export default GuessedWords
