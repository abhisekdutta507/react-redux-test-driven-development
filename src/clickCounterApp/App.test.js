import React from 'react'
import { shallow } from 'enzyme'

import App from './App'

/**
 * @description Factory function to create ShallowWrapper for the App component.
 * @function setup
 * @returns {ShallowWrapper}
 */
const setup = () => shallow(<App />)

const findByTestAttr = (wrapper, val) => wrapper.find(`[data-test="${val}"]`)

describe('rendering the app component', () => {
  const wrapper = setup()

  it('renders without crashing', () => {
    const appComponent = findByTestAttr(wrapper, 'component-app')
    // console.log(appComponent.debug())
    expect(appComponent.length).toBe(1)
  })

  it('renders button', () => {
    const button = findByTestAttr(wrapper, 'increment-button')
    expect(button.length).toBe(1)
  })

  it('renders counter display', () => {
    const counterDisplay = findByTestAttr(wrapper, 'counter-display')
    expect(counterDisplay.length).toBe(1)
  })

  it('counter starts at 0', () => {
    const count = findByTestAttr(wrapper, 'count')
    expect(count.text()).toBe('0')
  })

  it('clicking on imcrement button increments counter display', () => {
    // find the button
    const button = findByTestAttr(wrapper, 'increment-button')

    // click the button
    button.simulate('click')

    // find the count, and test that the number has been increased or not
    const count = findByTestAttr(wrapper, 'count')
    expect(count.text()).toBe('1')
  })

  it('clicking on decrement button decrements counter display', () => {
    // find the button
    const button = findByTestAttr(wrapper, 'decrement-button')

    // click the button
    button.simulate('click')

    // find the count, and test that the number has been increased or not
    const count = findByTestAttr(wrapper, 'count')
    expect(count.text()).toBe('0')
  })
})
