import React, { Component } from 'react'

import './App.css'

class App extends Component {
  state = {
    title: 'Jest',
    counter: 0,
    error: false
  }

  incrementCounter = () => {
    const { counter } = this.state
    this.setState({
      counter: counter + 1,
      error: false
    })
  }

  decrementCounter = () => {
    const { counter } = this.state
    this.setState({
      counter: counter > 0 ? counter - 1 : 0,
      error: counter === 0
    })
  }

  render() {
    const { title, counter, error } = this.state

    return (
      <div className="App" data-test="component-app">
        <h3>Learn react testing with {title}</h3>
        <p data-test="counter-display">Count: <span data-test="count">{counter}</span></p>
        {
          error && <p data-test="counter-error">Can't decrease anymore!!</p>
        }

        <button data-test="increment-button" onClick={this.incrementCounter}>Increment</button>

        <button data-test="decrement-button" onClick={this.decrementCounter}>Decrement</button>
      </div>
    )
  }
}

export default App
