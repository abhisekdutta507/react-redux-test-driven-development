import React, { Component } from 'react'
import { connect } from 'react-redux'
import './App.css'

import GuessedWords from './GuessedWords'
import Congrats from './Congrats'
import Input from './Input'
import { getSecretWord } from './actions'

export class UnconnectedApp extends Component {
  state = {}

  /**
   * @method componentDidMount
   * @returns {undefined}
   */
  componentDidMount() {
    const { getSecretWord } = this.props

    getSecretWord()
  }

  render() {
    const { success, secretWord, guessedWords } = this.props

    return (
      <div className="App container-fluid" data-test="component-app">
        <h4>Jotto - Udemy - Bonnie Schulkin</h4>
        <h6>The secret word is <span className="badge badge-secondary">{secretWord}</span></h6>
        <Congrats success={success} />
        <Input />
        <GuessedWords guessedWords={guessedWords} />
      </div>
    )
  }
}

const mapStateToProps = ({ success, guessedWords, secretWord }) => {
  return { success, guessedWords, secretWord }
}

export default connect(mapStateToProps, { getSecretWord })(UnconnectedApp)
