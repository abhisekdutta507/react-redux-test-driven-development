import React, { Component } from 'react'
import { connect } from 'react-redux'

import { guessWord } from './actions'

export class UnconnectedInput extends Component {
  state = {
    currentGuess: ''
  }

  submitGuessWord = (e) => {
    e.preventDefault()

    const { currentGuess } = this.state
    const { guessWord } = this.props

    if (!!currentGuess?.length) {
      guessWord(currentGuess)
      this.setState({ currentGuess: '' })
    }

  }

  render() {
    const { currentGuess } = this.state
    const { success } = this.props
    const contents = success
      ? null
      : (
        <form className="form-inline">
          <input data-test={'input-box'} className={'mb-2 mr-sm-3 form-control'} type="text" value={currentGuess} placeholder="Enter guess" onChange={(e) => this.setState({ currentGuess: e.target.value })} />
          <button data-test={'submit-button'} className={'btn btn-primary mb-2'} type="submit" onClick={this.submitGuessWord}>Submit</button>
        </form>
      )

    return (
      <div data-test={'component-input'}>{contents}</div>
    )
  }
}

const mapStateToProps = ({ success }) => {
  return { success }
}

export default connect(mapStateToProps, {
  guessWord
})(UnconnectedInput)
