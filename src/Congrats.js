import React from 'react'
import PropTypes from 'prop-types'

/**
 * Functional React component for congratulatory message.
 * @function
 * @param {object} props - React props.
 * @returns {JSX.Element} Rendered component (or null if `success` props is false).
 */
const Congrats = (props = {}) => {
  const { success } = props

  return (
    <div data-test="component-congrats">
      {
        !!success ?
          <div data-test="congrats-message" className="alert alert-success">Congratulations! you guessed the word!</div> :
          ''
      }
    </div>
  )
}

Congrats.propTypes = {
  success: PropTypes.bool.isRequired
}

export default Congrats
