/**
 * @method getLetterMatchCount
 * @param {string} guessedWord - Guessed word.
 * @param {*} secretWord - Secret word.
 * @returns {number} - Number of letters matched between guessed word and returns the number.
 */
export function getLetterMatchCount(guessedWord, secretWord) {
  const guessedLetterSet = new Set(guessedWord.split(''))
  const secretLetterSet = new Set(secretWord.split(''))

  // count the number of letters matches
  return [...secretLetterSet].filter((letter) => guessedLetterSet.has(letter)).length
}