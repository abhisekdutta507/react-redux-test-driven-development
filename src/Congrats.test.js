import React from 'react'
import { shallow } from 'enzyme'

import { findByTestAttr, checkProps } from '../test/testUtils'
import Congrats from './Congrats'

const defaultProps = { success: false }

/**
 * @description Factory function to create ShallowWrapper for the App component.
 * @function setup
 * @returns {ShallowWrapper}
 */
const setup = (props = {}) => {
  const setupProps = { ...defaultProps, ...props }
  return shallow(<Congrats {...setupProps} />)
}

describe('rendering the Congrats component', () => {

  it('renders without crashing', () => {
    const wrapper = setup()
    const component = findByTestAttr(wrapper, 'component-congrats')
    expect(component.length).toBe(1)
  })

  it('renders no text when `success` prop is falsy', () => {
    const wrapper = setup({ success: false })
    const component = findByTestAttr(wrapper, 'component-congrats')
    expect(component.text()).toBe('')
  })

  it('renders non empty congrats message when `success` prop is truthy', () => {
    const wrapper = setup({ success: true })
    const message = findByTestAttr(wrapper, 'congrats-message')
    expect(message.text().length).not.toBe(0)
  })

  it('does not throw warning with expected props', () => {
    checkProps(Congrats, defaultProps)
  })
})