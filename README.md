This project is created with [Create React App](https://github.com/facebook/create-react-app). We are using [Jest](https://jestjs.io/) and [Enzyme](https://enzymejs.github.io/enzyme/docs/guides/jest.html) in the project to setup the TDD (Test Driven Development) environment.

Press  `Cmd + Shift + V`  form your Mac keyboard to preview the README.md.

## Available Scripts

In the project directory, you can run:

### `npm test`

It will run the test cases that are created with Jest.

## Learn More

You can learn more in the [Udemy Tutorial](https://www.udemy.com/course/react-testing-with-jest-and-enzyme/learn/lecture/10531816#overview).

### Simple Jest & Enzyme Setup In A React Project

**Initialize the project with `NPM`.** *Make sure you have a stable version of* [NodeJS](https://nodejs.org/en/), [NPM](https://www.npmjs.com/) & [Create React App](https://github.com/facebook/create-react-app) *is installed in your system globally.*

```bash
npx create-react-app react-redux-testing-jest
```

Wait for few minutes while generating the new project. Then move into the project directory using `cd` command.

```bash
cd react-redux-testing-jest

npm test
```

**Check** the `App.test.js` file to understand the test cases. Visit [App.test.js](https://bitbucket.org/abhisekdutta507/react-redux-test-driven-development/src/master/src/App.test.js). Please visit [ShallowWrapper](https://enzymejs.github.io/enzyme/docs/api/ShallowWrapper/find.html) & [ReactWrapper](https://enzymejs.github.io/enzyme/docs/api/ReactWrapper/simulate.html) for more info.


### Install the following dependencies

Install `enzyme`, `jest-enzyme`, `@wojtekmaj/enzyme-adapter-react-17` & `check-prop-types` as developer dependencies along with `prop-types` as dependency.

```bash
npm i prop-types

npm i enzyme jest-enzyme @wojtekmaj/enzyme-adapter-react-17 check-prop-types -D
```

### Steps to write test cases with less code

**Description** - A **Utility** will help you write less codes.

* **Step 1** — create the [testUtils.js](https://bitbucket.org/abhisekdutta507/react-redux-test-driven-development/src/master/test/testUtils.js) file inside the project root directory. Then add the below codes:

```js
import checkPropTypes from 'check-prop-types'

/**
 * Return node(s) with given data-test attribute.
 * @param {ShallowWrapper} wrapper - Enzyme shallow wrapper.
 * @param {string} val - Value of the data-test attribute for search.
 * @returns {ShallowWrapper}
 */
export const findByTestAttr = (wrapper, val) => wrapper.find(`[data-test="${val}"]`)

/**
 * Matches the component props with expected props
 * @param {*} component 
 * @param {*} conformingProps 
 */
export const checkProps = (component, conformingProps) => {
  const propError = checkPropTypes(
    component.propTypes,
    conformingProps,
    'prop',
    component.name
  )

  expect(propError).toBeUndefined()
}
```

* **Step 2** — Add the imports in an existing file [Congrats.test.js](https://bitbucket.org/abhisekdutta507/react-redux-test-driven-development/src/master/src/Congrats.test.js).

```js
import React from 'react'

import { shallow } from 'enzyme'

import { findByTestAttr, checkProps } from '../test/testUtils'

import Congrats from './Congrats'
```

* **Step 3** — create `defaultProps` for props testing.

```js
const defaultProps = { success: false }
```

* **Step 4** — create the `props` testing setup.

```js
/**
 * @description Factory function to create ShallowWrapper for the App component.
 * @function setup
 * @returns {ShallowWrapper}
 */
const setup = (props = {}) => {
  const setupProps = { ...defaultProps, ...props }
  return shallow(<Congrats {...setupProps} />)
}
```

* **Step 5** — add your first test case.

```js
...

describe('rendering the Congrats component', () => {
  ...

  it('renders without crashing', () => {
    const wrapper = setup()
    const component = findByTestAttr(wrapper, 'component-congrats')
    expect(component.length).toBe(1)
  })

  ...
})
```

* **Step 6** — add test cases to compare the props.

```js
...

describe('rendering the Congrats component', () => {
  ...

  it('does not throw warning with expected props', () => {
    checkProps(Congrats, defaultProps)
  })
})
```

Hurray!! we have learned the basic concepts of Jest testing setup in React.js project. Our goal will be "DRY i.e. Don't Repeat Yourself"

npm i redux react-redux

For server application please clone [random-word-server](https://bitbucket.org/abhisekdutta507/random-word-server-for-react-redux-testing-jest/src/master/)
